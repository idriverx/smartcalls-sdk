<?php


namespace App\Api;


class ScenarioVariablesProvider
{
    /**
     * @var QueryInterface
     */
    protected $provider;

    /**
     * ScenarioVariablesProvider constructor.
     * @param QueryInterface $provider
     */
    public function __construct(QueryInterface $provider)
    {
        $this->provider = $provider;
    }

    /**
     * @param string $accessToken
     * @param string $phone
     * @param int $scenarioId
     * @param string $domain
     * @return mixed
     */
    public function getScenarioVariables(string $accessToken, string $phone, int $scenarioId, string $domain)
    {
        $response = $this->provider->send('scenario/getScenarioVariables', [
            'domain' => $domain,
            'access_token' => $accessToken,
            'scenario_id' => $scenarioId,
            'phone' => $phone
        ]);
        return $response;
    }

}