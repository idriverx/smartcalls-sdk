<?php


namespace App\Api;

interface QueryInterface
{
    const ENDPOINT = 'https://smartcalls.io/api/v2/';

    public function send(string $url, array $fields = []);
}