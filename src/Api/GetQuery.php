<?php


namespace App\Api;

class GetQuery implements QueryInterface
{
    public function send(string $url, array $fields = [])
    {
        $result = self::ENDPOINT . $url;

        if (!empty($fields)) {
            $result .= '?';
            $result .= http_build_query($fields);
        }

        return file_get_contents($result);
    }
}