<?php


namespace App\Api;

class RunScenario
{
    /**
     * @var QueryInterface
     */
    protected $provider;

    /**
     * RunScenario constructor.
     * @param QueryInterface $provider
     */
    public function __construct(QueryInterface $provider)
    {
        $this->provider = $provider;
    }

    /**
     * @param string $accessToken
     * @param string $phone
     * @param string|null $variablesJson
     * @param int $scenarioId
     * @param string $domain
     * @param string $callFromNumber
     * @return bool
     */
    public function run(
        string $accessToken,
        string $phone,
        ?string $variablesJson,
        int $scenarioId,
        string $domain,
        string $callFromNumber
    ) : bool
    {
        $response = $this->provider->send('scenario/runScenario', [
            'domain' => $domain,
            'access_token' => $accessToken,
            'scenario_id' => $scenarioId,
            'phone' => $phone,
            'variables' => $variablesJson,
            'phone_number_id' => $callFromNumber
        ]);

        if ($response) {
            $response = json_decode($response);
            return $response->success;
        }

        return false;
    }

}