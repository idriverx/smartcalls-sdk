<?php


namespace App\Api;

class PostQuery implements QueryInterface
{
    public function send(string $url, array $fields = [])
    {
        $ch = curl_init(self::ENDPOINT . $url);
        curl_setopt_array($ch, [
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $fields
            ]
        );
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
}