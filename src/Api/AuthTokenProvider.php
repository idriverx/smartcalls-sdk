<?php


namespace App\Api;

class AuthTokenProvider
{
    /**
     * @var QueryInterface
     */
    protected $provider;

    /**
     * @var array
     */
    protected $config;

    /**
     * AuthToken constructor.
     * @param $provider
     * @param array $config
     */
    public function __construct($provider, array $config)
    {
        $this->provider = $provider;
        $this->config = $config;
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function getToken()
    {
        $response = $this->provider->send('auth/getAccessToken', $this->config);
        if (!$response) {
            throw new \Exception("Auth failed");
        }
        $response = json_decode($response);
        if ($response->success === false || !$response->result) {
            throw new \Exception("Auth failed");
        }
        return $response->result;
    }
}
